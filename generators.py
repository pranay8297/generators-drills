#problem 1
def gen_range(start, stop, step):
    value = start
    for i in range(start , stop , step):
        yield value
        value += step

ra = gen_range(2 , 20 , 4)
for i in ra:
    print(i)
    
    
#problem 2
def gen_zip(*args):
    for i in range(len(args[0])):
        temp = []
        for j in args:
            temp.append(j[i])
        yield temp
        
a = gen_zip([1 , 2],[1 , 2])
for i in a:
    print(i)
