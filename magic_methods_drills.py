import random

#problem - 1 using a class 
class Adding:
    def __init__(self , num):
        self.num = num
    
    def __add__(self , num2):
        return self.num + num2
    
num1 = Adding(3)
num2 = 8
print(num1 + num2)

#problem - 1

def add(c , d):
    return c.__add__(d)

#problem - 2  
lis = [1 , 3 , 5 , 6]
def length(lis):
    return lis.__len__()
print(length(lis))

## problem 3
string = 'lorem ipsum sit amet'
def string_length(string):
    return string.__len__()
print(string_length(string))

##problem 4

lis1 = [1 , 3 , 5 , 7]
lis2 = [2 , 4 , 6 , 8]
def add_list(lis1 , lis2):
    return lis1.__add__(lis2)
print(add_list(lis1 , lis2))

#problem 5

class BankAccount:
    def __init__(self, id, balance):
        self.id = id
        self.balance = balance

    def __repr__(self):
        return "BankAccount(id={}, balance={})".format(self.id, self.balance)
    
    def __lt__(self , other):
        return self.balance < other.balance
    
    def __gt__(self , other):
        return self.balance > other.balance
    
    def __eq__(self , other):
        return self.balance == other.balance and self.id == other.id
    
    def __repr__(self):
        return '(id : {}, balance = {})'.format(self.id , self.balance)
    
    def compare(self , other):
        return self == other
            

bank_accounts = [BankAccount(id = 1 , balance = random.randint(1 , 100000)) for i in range(100)]
bank_accounts.sort()
for i in bank_accounts:
    print(i)


# problem 6

a1 = BankAccount(1 , 30984)
a2 = BankAccount(2 , 98332)
a1.compare(a2)
a1 == a2

#problem 7
#custom iterator

class Bank:
    def __init__(self):
        self.accounts = []

    def add_account(self, account):
        self.accounts.append(account)
    
    def __iter__(self):
        self.n = 0
        return self
    
    def __next__(self):
        if self.n < len(self.accounts) - 1:
            self.n += 1
            return self.accounts[self.n]
        else:
            raise StopIteration


bank = Bank()
for i in range(100):
    bank.add_account(BankAccount(id=i, balance=i * 100))

for account in bank:
    print(account)


















